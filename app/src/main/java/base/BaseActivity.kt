package com.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.base.di.component.AppComponent
import com.base.mvp.BasePresenter
import com.base.mvp.BaseView
import mobileparadigm.arkangelx.com.buzzmovetest.home.model.places.Result
import mobileparadigm.arkangelx.com.buzzmovetest.home.presenter.RxBus
import org.jetbrains.anko.toast

/**
 * Created by Burak Eregar on 20.05.2017.
 * burakeregar@gmail.com
 * https://github.com/burakeregar
 */
abstract class BaseActivity : AppCompatActivity(), BaseView {

    private var presenter: BasePresenter<*>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onActivityInject()
    }

    protected abstract fun onActivityInject()

    fun getAppcomponent(): AppComponent = App.appComponent

    override fun setPresenter(presenter: BasePresenter<*>) {
        this.presenter = presenter
    }

    override fun onError() {
        toast("Something went wrong")
    }


    override fun onDestroy() {
        super.onDestroy()
        presenter?.detachView()
        presenter = null
    }



}
