package com.base.di.module

import android.content.res.Resources
import com.base.App
import dagger.Module
import dagger.Provides
import mobileparadigm.arkangelx.com.buzzmovetest.BuildConfig
import mobileparadigm.arkangelx.com.buzzmovetest.R
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton


@Module
class RetrofitModule {
    @Provides
    @Named("PlacesRetrofit")
    @Singleton
    fun providesPlacesRetrofit(client: OkHttpClient, resources: Resources): Retrofit = Retrofit.Builder().baseUrl(resources.getString(R.string.PLACES_BASE_URL))
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(client)
            .build()

    @Provides
    @Singleton
    @Named("GeoCodeRetrofit")
    fun providesGeoCodeRetrofit(client: OkHttpClient, resources: Resources): Retrofit = Retrofit.Builder().baseUrl(resources.getString(R.string.GEOCODE_BASE_URL))
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(client)
            .build()
}