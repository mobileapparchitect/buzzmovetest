package com.base.di.module

import android.content.res.Resources
import dagger.Module
import dagger.Provides
import mobileparadigm.arkangelx.com.buzzmovetest.R
import mobileparadigm.arkangelx.com.buzzmovetest.api.GeocodeApiService
import mobileparadigm.arkangelx.com.buzzmovetest.api.PlacesSearchApi
import mobileparadigm.arkangelx.com.buzzmovetest.api.RestClientImplementation
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton


@Module
class ApiModule {
    @Provides
    @Singleton
    fun providesPlacesSearch(@Named("PlacesRetrofit") retrofit: Retrofit): PlacesSearchApi = retrofit.create(PlacesSearchApi::class.java)

    @Provides
    @Singleton
    fun providesGeocodeApiService(@Named("GeoCodeRetrofit") retrofit: Retrofit): GeocodeApiService = retrofit.create(GeocodeApiService::class.java)


    @Provides
    @Singleton
    fun provideRestClientImplementation(placesSearchApi: PlacesSearchApi, geocodeApiService: GeocodeApiService, resources: Resources):
            RestClientImplementation = RestClientImplementation(geocodeApiService, placesSearchApi,resources.getString(R.string.API_KEY))


}