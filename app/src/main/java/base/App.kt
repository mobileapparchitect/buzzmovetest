package com.base

import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex
import com.base.di.component.AppComponent
import com.base.di.component.DaggerAppComponent

import com.base.di.module.AppModule
import com.facebook.drawee.backends.pipeline.Fresco


class App: Application() {

    companion object{
       @JvmStatic lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        initDagger()
        Fresco.initialize(this)
    }

    private fun initDagger() {
        appComponent = DaggerAppComponent.builder().appModule(AppModule(this)).build()
    }

     override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
         MultiDex.install(this)
    }

}