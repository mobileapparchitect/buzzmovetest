package mobileparadigm.arkangelx.com.buzzmovetest.home.model.geocode

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LocationResponse {

    @SerializedName("location")
    @Expose
    var location: Location? = null
    @SerializedName("accuracy")
    @Expose
    var accuracy: Int? = null


}
