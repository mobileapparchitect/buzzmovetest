package mobileparadigm.arkangelx.com.buzzmovetest.home.presenter

import com.base.mvp.BaseView
import mobileparadigm.arkangelx.com.buzzmovetest.home.model.places.Result

interface HomeView : BaseView {
    fun onSearchResponse(list: List<Result>?)
    fun showProgress()
    fun hideProgress()
    fun noResult()
}
