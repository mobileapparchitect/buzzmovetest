package mobileparadigm.arkangelx.com.buzzmovetest.api

import io.reactivex.Observable
import mobileparadigm.arkangelx.com.buzzmovetest.home.model.geocode.LocationResponse
import mobileparadigm.arkangelx.com.buzzmovetest.home.model.places.PlaceSearchResponse
import retrofit2.http.Query

interface RestClient {

    fun getCurrentLocation(): Observable<LocationResponse>

    fun findAPlace(
            @Query("location") location: String,
            @Query("radius") lattitudeLongitude: String,
            @Query("key") apiKey: String
    )


            : Observable<PlaceSearchResponse>
}