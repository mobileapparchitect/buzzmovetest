package mobileparadigm.arkangelx.com.buzzmovetest


import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.base.BaseActivity
import com.base.helper.SharedPreferencesHelper
import com.burakeregar.githubsearch.home.di.DaggerHomeActivityComponent
import com.burakeregar.githubsearch.home.di.HomeActivityModule
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.mancj.materialsearchbar.MaterialSearchBar
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import kotlinx.android.synthetic.main.activity_main.*
import mobileparadigm.arkangelx.com.buzzmovetest.home.model.places.Result
import org.jetbrains.anko.toast
import javax.inject.Inject
import com.google.android.gms.maps.model.MarkerOptions
import mobileparadigm.arkangelx.com.buzzmovetest.home.presenter.*


class HomeActivity : BaseActivity(), HomeView, MaterialSearchBar.OnSearchActionListener, OnMapReadyCallback {


    lateinit var mapView: MapView
    private var gmap: GoogleMap? = null

    @Inject
    lateinit var presenter: HomePresenter

    @Inject
    lateinit var   sharedPreferencesHelper: SharedPreferencesHelper

    @Inject
    lateinit var inputMethodManager: InputMethodManager

    private lateinit var searchResultAdapter: SearchResultAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initAdapter()
        initSearchView()
        MapsInitializer.initialize(this);
        mapView = findViewById(R.id.map_view)
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(OnMapReadyCallback { googleMap ->
            gmap = googleMap

        })
        RxBus.listen(Result::class.java).subscribe({ result ->
            aninmateCamera(result)
        }
        )


    }

    override fun onResume() {
        super.onResume()
        presenter.getUserCurrentLocation()
        mapView.onResume()
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        gmap = googleMap;
    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
    }


    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }


    override fun onPause() {
        mapView.onPause()
        super.onPause()
    }

    override fun onDestroy() {
        mapView.onDestroy()
        super.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }


    private fun initAdapter() {

        with(homeRv) {
            homeRv.layoutManager = android.support.v7.widget.LinearLayoutManager(this@HomeActivity, android.widget.LinearLayout.VERTICAL, false)
            searchResultAdapter = SearchResultAdapter()
            homeRv.adapter = searchResultAdapter
        }

    }


    private fun initSearchView() {
        with(homeSearchView) {
            setHint(getString(R.string.searchview_hint))
            setSpeechMode(true)
            setOnSearchActionListener(this@HomeActivity)
        }
    }

    override fun onActivityInject() {
        DaggerHomeActivityComponent.builder().appComponent(getAppcomponent())
                .homeActivityModule(HomeActivityModule())
                .build()
                .inject(this)

        presenter.attachView(this)
    }

    override fun showProgress() {
        homeRv.visibility = View.GONE
        homeShimmer.visibility = View.VISIBLE
        homeShimmer.startShimmerAnimation()
    }

    override fun hideProgress() {
        homeRv.visibility = View.VISIBLE
        homeShimmer.visibility = View.GONE
        homeShimmer.stopShimmerAnimation()
    }

    override fun onSearchResponse(list: List<Result>?) {
        searchResultAdapter.addItems(list!!)
    }


    override fun onButtonClicked(buttonCode: Int) {}

    override fun onSearchStateChanged(enabled: Boolean) {}

    override fun onSearchConfirmed(text: CharSequence?) {
        with(homeSearchView) {
            hideKeyboard(homeSearchView)
        }
        presenter.executePlaceSearch(text?.toString() ?: "")
    }

    override fun noResult() {
        toast("We couldn't find any places close to you.")
    }

    private fun hideKeyboard(v: View) {
        inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    fun aninmateCamera(result: Result) {
        with(sliding_layout) {
            sliding_layout.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
        }
        gmap?.setMinZoomPreference(5.0f);
        var place = LatLng(result.geometry?.location?.lat!!.toDouble(), result.geometry?.location?.lng!!.toDouble());
        val location = CameraUpdateFactory.newLatLngZoom(
                place, 15f)

        gmap?.addMarker(MarkerOptions().position(place)
                .title(result.name))

        var currentLattitude: Float = sharedPreferencesHelper.getDataFromSharedPref(Float::class.java, "CurrentLattitude") as Float
        var currentLongitude: Float = sharedPreferencesHelper.getDataFromSharedPref(Float::class.java, "CurrentLongitude") as Float
        var userCurrentLocation = LatLng(currentLattitude.toDouble(),currentLongitude.toDouble())


        gmap?.addMarker(MarkerOptions().position(userCurrentLocation)
                .title("You are here"))
        gmap?.animateCamera(location)

        //missing way points hence straight line could be cool 2 implement
        MapAnimator.getInstance().animateRoute(gmap, listOf(place,userCurrentLocation))
    }


}
