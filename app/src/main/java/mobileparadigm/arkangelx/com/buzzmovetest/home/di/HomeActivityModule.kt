package com.burakeregar.githubsearch.home.di

import com.base.di.ActivityScope
import com.base.helper.SharedPreferencesHelper
import com.base.util.AppSchedulerProvider
import mobileparadigm.arkangelx.com.buzzmovetest.home.presenter.HomePresenter
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import mobileparadigm.arkangelx.com.buzzmovetest.api.RestClientImplementation

@Module
class HomeActivityModule {

    @Provides
    @ActivityScope
    internal fun providesHomePresenter(restClientImplementation: RestClientImplementation, spHelper: SharedPreferencesHelper, disposable: CompositeDisposable, scheduler: AppSchedulerProvider): HomePresenter =
            HomePresenter(restClientImplementation, spHelper, disposable, scheduler)
}
