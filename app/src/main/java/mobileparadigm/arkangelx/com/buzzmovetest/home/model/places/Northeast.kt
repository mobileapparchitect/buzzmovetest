package mobileparadigm.arkangelx.com.buzzmovetest.home.model.places

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Northeast {

    @SerializedName("lat")
    @Expose
    var lat: Float? = null
    @SerializedName("lng")
    @Expose
    var lng: Float? = null

}
