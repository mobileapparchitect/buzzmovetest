package com.burakeregar.githubsearch.home.di

import com.base.di.ActivityScope
import com.base.di.component.AppComponent
import mobileparadigm.arkangelx.com.buzzmovetest.HomeActivity

import dagger.Component

@ActivityScope
@Component(dependencies = arrayOf(AppComponent::class), modules = arrayOf(HomeActivityModule::class))
interface HomeActivityComponent {

    fun inject(homeActivity: HomeActivity)
}
