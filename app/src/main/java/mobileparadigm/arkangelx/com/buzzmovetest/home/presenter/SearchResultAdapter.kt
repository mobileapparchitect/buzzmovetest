package mobileparadigm.arkangelx.com.buzzmovetest.home.presenter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.facebook.drawee.view.SimpleDraweeView
import mobileparadigm.arkangelx.com.buzzmovetest.R
import mobileparadigm.arkangelx.com.buzzmovetest.home.model.places.Result


class SearchResultAdapter() : RecyclerView.Adapter<SearchResultAdapter.ViewHolder>() {
    private var itemList: ArrayList<Result?>? = null

    init {
        itemList = ArrayList()
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent?.context).inflate(R.layout.view_search_result_item, parent, false)
        return ViewHolder(v);
    }

    fun addItems(baseResponseList: List<Result>) {
        itemList?.addAll(baseResponseList)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return itemList?.size!!
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val itemAtPosition = itemList!!.get(position)!!
        holder?.placeName?.text = itemAtPosition.name
        holder?.placeAddress?.text = itemAtPosition.formattedAddress
        holder?.placeName?.text = itemAtPosition.name
        holder?.placeImge?.setImageURI(itemAtPosition.icon)
        holder?.rootView?.setOnClickListener({ _ ->
            RxBus.publish(itemAtPosition)
        }
        )


    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val placeName = itemView.findViewById<TextView>(R.id.placename)
        val placeAddress = itemView.findViewById<TextView>(R.id.placeaddress)
        val placeType = itemView.findViewById<TextView>(R.id.placetype)
        val rootView = itemView.findViewById<LinearLayout>(R.id.root)
        val placeImge = itemView.findViewById<SimpleDraweeView>(R.id.placeImage)

    }
}