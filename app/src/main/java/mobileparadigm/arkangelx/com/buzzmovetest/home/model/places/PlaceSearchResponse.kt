package mobileparadigm.arkangelx.com.buzzmovetest.home.model.places

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PlaceSearchResponse {

    @SerializedName("html_attributions")
    @Expose
    var htmlAttributions: List<Any>? = null
    @SerializedName("results")
    @Expose
    var results: List<Result>? = null
    @SerializedName("status")
    @Expose
    var status: String? = null

}
