package mobileparadigm.arkangelx.com.buzzmovetest.api

import io.reactivex.Observable
import mobileparadigm.arkangelx.com.buzzmovetest.home.model.geocode.LocationResponse
import mobileparadigm.arkangelx.com.buzzmovetest.home.model.places.PlaceSearchResponse

class RestClientImplementation(geocodeApiService: GeocodeApiService, placesSearchApi: PlacesSearchApi, apiKey: String) : RestClient {
    var geocodeApiService: GeocodeApiService?
    var placesSearchApi: PlacesSearchApi?
    var apiKey: String?

    init {
        this.geocodeApiService = geocodeApiService
        this.placesSearchApi = placesSearchApi
        this.apiKey = apiKey
    }

    override fun getCurrentLocation(): Observable<LocationResponse> {
        return geocodeApiService!!.getCurrentLocation(apiKey!!)
    }

    override fun findAPlace(query: String, location: String, radius: String): Observable<PlaceSearchResponse> {
        return placesSearchApi!!.findAPlace(query, location, radius, this.apiKey!!)
    }

}