package mobileparadigm.arkangelx.com.buzzmovetest.api

import io.reactivex.Observable
import mobileparadigm.arkangelx.com.buzzmovetest.home.model.geocode.LocationResponse
import retrofit2.http.POST
import retrofit2.http.Query

interface GeocodeApiService {


    //https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyB4WHOLTsEq9gUzdRp2a7vFf-VeKNRe4b0
    @POST("geolocate")
    fun getCurrentLocation(@Query("key") key: String): Observable<LocationResponse>
}
