package mobileparadigm.arkangelx.com.buzzmovetest.api

import io.reactivex.Observable
import mobileparadigm.arkangelx.com.buzzmovetest.home.model.places.PlaceSearchResponse
import retrofit2.http.GET
import retrofit2.http.Query


interface PlacesSearchApi {
    @GET("place/textsearch/json")
    fun findAPlace(
            @Query("query") query: String,
            @Query("location") location: String,
            @Query("radius") lattitudeLongitude: String,
            @Query("key") key: String
    )


            : Observable<PlaceSearchResponse>
}