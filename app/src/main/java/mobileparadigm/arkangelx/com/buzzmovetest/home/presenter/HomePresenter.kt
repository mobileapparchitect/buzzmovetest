package mobileparadigm.arkangelx.com.buzzmovetest.home.presenter

import com.base.helper.SharedPreferencesHelper
import com.base.mvp.BasePresenter
import com.base.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import mobileparadigm.arkangelx.com.buzzmovetest.api.RestClientImplementation
import mobileparadigm.arkangelx.com.buzzmovetest.home.model.geocode.LocationResponse
import mobileparadigm.arkangelx.com.buzzmovetest.home.model.places.PlaceSearchResponse
import javax.inject.Inject

class HomePresenter @Inject constructor(var restClientImplementation: RestClientImplementation, var sharedPreferencesHelper: SharedPreferencesHelper, disposable: CompositeDisposable, scheduler: SchedulerProvider) : BasePresenter<HomeView>(disposable, scheduler) {


    fun getUserCurrentLocation() {
        view?.showProgress()
        disposable.add(restClientImplementation.getCurrentLocation()
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui()).subscribe({ locationRespsone: LocationResponse? ->
                    view?.hideProgress()
                    sharedPreferencesHelper.putDatatoSharedPref(convertToFloat(locationRespsone!!.location?.lat), Float::class.java, "CurrentLattitude")
                    sharedPreferencesHelper.putDatatoSharedPref(convertToFloat(locationRespsone!!.location?.lng), Float::class.java, "CurrentLongitude")
                },
                        { _ ->
                            view?.hideProgress()
                            view?.onError()
                        })


        )
    }

    fun executePlaceSearch(query: String) {
        var currentLattitude: Float = sharedPreferencesHelper.getDataFromSharedPref(Float::class.java, "CurrentLattitude") as Float
        var currentLongitude: Float = sharedPreferencesHelper.getDataFromSharedPref(Float::class.java, "CurrentLongitude") as Float
        view?.showProgress()
        disposable.add(restClientImplementation.findAPlace(query, currentLattitude.toString() + "," + currentLongitude.toString(), "500")
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui()).subscribe({ placeSearchResponse: PlaceSearchResponse? ->
                    view?.onSearchResponse(placeSearchResponse?.results)
                    view?.hideProgress()
                })

        )
    }


    private fun convertToFloat(doubleValue: Double?): Float? {
        return doubleValue?.toFloat()
    }

}